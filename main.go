package main

import (
	"ProductQueryService/elasticsearch"
	"ProductQueryService/product"
	"log"
)

func main() {
	es, err := elasticsearch.Connect()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	err = product.GetOne(es, "57ec14e8-6759-404f-b79c-570a22aaca13")
	if err != nil {
		log.Fatalf("Error cannot get one product: %s", err)
	}

	err = product.Search(es)
	if err != nil {
		log.Fatalf("Error cannot search products: %s", err)
	}
}
