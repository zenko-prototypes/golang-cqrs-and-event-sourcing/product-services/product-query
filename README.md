# CQRS Query

## Description

This microservice is here to handle query and retrieve data from the elastic search database. 
His purpose is to handle read data.
It needs to be started last.

https://gitlab.com/zenko-prototypes/golang-cqrs-and-event-sourcing/product-services/product-projector
https://gitlab.com/zenko-prototypes/golang-cqrs-and-event-sourcing/product-services/product-command
https://gitlab.com/zenko-prototypes/golang-cqrs-and-event-sourcing/product-services/product-query

## Installation

You need to update information about elastic search connection in this [file](elasticsearch/elasticsearch.go)

## Run

```bash
$ go run main.go
```