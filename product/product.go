package product

import (
	"bytes"
	"github.com/elastic/go-elasticsearch/v8"
	"unsafe"
)

type Product struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func GetOne(client *elasticsearch.Client, id string) error {
	res, err := client.Get("product", id)
	if err != nil {
		return err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(res.Body)
	if err != nil {
		return err
	}
	b := buf.Bytes()
	s := *(*string)(unsafe.Pointer(&b))
	println(s)

	return res.Body.Close()
}

func Search(client *elasticsearch.Client) error {
	res, err := client.Search(client.Search.WithIndex("product"))
	if err != nil {
		return err
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(res.Body)
	if err != nil {
		return err
	}
	b := buf.Bytes()
	s := *(*string)(unsafe.Pointer(&b))
	println(s)

	return res.Body.Close()
}
